import tweepy

class Tweet:
    def __init__(self, consumer_key: str, consumer_secret: str, access_token: str, access_token_secret: str):
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.access_token = access_token
        self.access_token_secret = access_token_secret

    def auth(self) -> tweepy.API:
        auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        auth.set_access_token(self.access_token, self.access_token_secret)
        return tweepy.API(auth, wait_on_rate_limit=True)

    def generate_tweet(
                    self, 
                    api: tweepy.API, 
                    keywords: str, 
                    lang: str, 
                    start_date: str, 
                    end_date: str, 
                    count: int) -> tweepy.Cursor:
        return tweepy.Cursor(
            api.search, 
            q=keywords, 
            lang=lang, 
            since=start_date, 
            until=end_date, 
            tweet_mode="extended"
        ).items(count)