from flask import Flask, render_template, request, redirect, url_for
from dotenv import load_dotenv
import os
import tweepy
import app.lib.tweet as twitter_api

load_dotenv()

app = Flask(__name__)

tweepy_api = twitter_api.Tweet(
        os.getenv('TWITTER_CONSUMER_KEY'),
        os.getenv('TWITTER_CONSUMER_SECRET'),
        os.getenv('TWITTER_ACCESS_TOKEN'),
        os.getenv('TWITTER_ACCESS_TOKEN_SECRET')
    )

@app.route("/", methods=['GET'])
def home_view():
    return render_template("home.html")

@app.route("/crawl", methods=['GET'])
def crawl_view():
    return render_template("crawl.html")

@app.route("/result", methods=['GET', 'POST'])
def result_view():
    if request.method == 'GET':
        return redirect(url_for('crawl'))
    
    tweet = tweepy_api.generate_tweet(
        tweepy_api.auth(),
        request.form['keywords'],
        "id",
        request.form['start_date'],
        request.form['end_date'],
        int(request.form['count'])
    )
    
    tweets = []
    for data in tweet:
        tweet_data = {
            'date': data.created_at,
            'text': data.full_text
        }
        tweets.append(tweet_data)

    return render_template("result.html", tweets=tweets)